<?php

namespace Modules\Auth\Services;

use App\Models\StoryBot;
use App\Models\Subscriber;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthService
{
    /**
     * User not authenticated
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function userNotAuth(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#notauth', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #notauth'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }

    /**
     * User is authenticated
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function userIsAuth(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#isauth', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #isauth'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }

    /**
     * Invalid parameter for authenticate
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function invalidAuthParameter(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#invalidparam', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #invalidparam'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }

    /**
     * Auth parameter not found in DB
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function authParameterNotFound(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#authparameternotfound', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #authparameternotfound'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }

    /**
     * OTP invalid (not found)
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function authOtpNotFound(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#authotpnotfound', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #authotpnotfound'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }

    /**
     * User successfully authenticated
     *
     * @param \App\Models\Subscriber $subscriber
     *
     * @return array
     */
    public function authenticated(Subscriber $subscriber)
    {
        try {
            $story = StoryBot::findByKeyword('#authenticated', $subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #authenticated'];
        }

        $subscriber->branch = $story->id;
        $subscriber->save();

        return ['branch' => true];
    }
}
