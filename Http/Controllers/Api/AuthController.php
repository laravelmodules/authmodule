<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\BotLogic\Dto\EventyData;
use App\Models\Subscriber;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Services\AuthService;
use TorMorten\Eventy\Facades\Eventy;

class AuthController extends Controller
{
    /**
     * @var \Modules\Auth\Services\AuthService
     */
    private $service;

    public function __construct()
    {
        $this->service = new AuthService();
    }

    /**
     * @param object|Request $request {
     *                      All subscribers data from jsonApi plugin
     *
     *                      @type int|string      $user_id
     *                      User id from jsonApi plugin
     * }
     *
     * @return array
     *   $subscriber->options()->updateOrCreate(
     *           [
     *               'optionable_id' => $subscriber->id,
     *               'parameter'     => UsersOptions::AUTHENTICATED,
     *           ],
     *           [
     *               'value' => true,
     *           ]
     *       );
     */

    public function __invoke(Request $request)
    {
        try {
            $subscriber = Subscriber::findOrFail($request->user_id);
        } catch (Exception $e) {
            return [
                'message' => 'Good luck'
            ];
        }

        return $this->authSubscriber($subscriber);
    }

    public function authSubscriber(Subscriber $subscriber)
    {
        //All auth logic in action need.auth
        Eventy::action('need.auth', new EventyData([
            'subscriber' => $subscriber
        ]));

        try {
            $subscriber->options()->where('parameter', 'auth')->firstOrFail();
            return $this->service->authenticated($subscriber);
        } catch (Exception $exception) {
            return $this->service->userNotAuth($subscriber);
        }
    }
}
