<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Enums\Options\UsersOptions;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Services\AuthService;

class CheckAuthController extends Controller
{
    /**
     * @param object|Request $request {
     *          All subscribers data from jsonApi plugin
     *
     *          @type int|string      $user_id
     *          User id from jsonApi plugin
     * }
     *
     * @return array
     */
    public function __invoke(Request $request)
    {
        $service = new AuthService();

        /** @var Subscriber $subscriber */
        $subscriber = Subscriber::with([
            'options' => function ($query) {
                $query->where('parameter', UsersOptions::AUTHENTICATED);
            },
        ])
            ->find($request->user_id);

        /** @var \Illuminate\Support\Collection $options */
        $options = optional($subscriber->options)->first();

        if (!$options) {
            return $service->userNotAuth($subscriber);
        }

        return $service->userIsAuth($subscriber);
    }
}
